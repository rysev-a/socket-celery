import json
from app.users.models import User


def test_get_users_list(session, client):
    for user_name in ['alex', 'yrii', 'alina']:
        new_user = User(email="{}@mail.ru".format(user_name), password='pass')
        session.add(new_user)
    session.commit()

    response = client.get('/api/v1/users')
    assert '200' in response.status
    assert len(response.json) > 0

def test_create_new_user(client):
    user_data = {'email': 'new@example.com', 'password': 'pa$$worD'}
    response = client.post('/api/v1/users', 
                            data=json.dumps(user_data), 
                            content_type='application/json')

    user = User.query.filter(User.email == user_data['email']).one()

    assert (user.email in response.json.get('email') and
            user.id == response.json.get('id'))

def test_get_user(user, client):
    response = client.get('/api/v1/users/{}'.format(user.id))
    assert response.json.get('id') is user.id
    assert response.json.get('email') in user.email

def test_update_user(user, client):
    update_data = {'email': 'new@mail.ru', 'first_name': 'alexey'}
    client.put('/api/v1/users/{}'.format(user.id),
        data=json.dumps(update_data),
        content_type='application/json')

    response = client.get('/api/v1/users/{}'.format(user.id))
    assert response.json.get('email') in update_data.get('email')

def test_delete_user(user, client):
    client.delete('/api/v1/users/{}'.format(user.id))
    assert User.query.filter_by(email=user.email).first() is None

def test_login_user(user, client):
    response = client.post('/api/v1/profile/login', 
                            data=json.dumps({'email': user.email,
                                             'password': 'pass'}), 
                            content_type='application/json')

    assert response.json.get('email') in user.email

def test_current_user(user, client):
    #login user
    client.post('/api/v1/profile/login',
                            data=json.dumps({'email': user.email,
                                             'password': 'pass'}),
                            content_type='application/json')

    response = client.get('/api/v1/profile/current')
    assert response.json.get('email') in user.email

def test_logout_user(client, user):
    #login user
    client.post('/api/v1/profile/login',
                            data=json.dumps({'email': user.email,
                                             'password': 'pass'}),
                            content_type='application/json')

    #logout user
    client.post('/api/v1/profile/logout')
    response = client.get('/api/v1/profile/current')
    assert '400' in response.status
