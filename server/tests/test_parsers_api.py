import json
from app.parsers.models import Parser


def test_get_parsers_list(session, client):
    for parser_domain in ['yandex.ru', 'google.com', 'rambler.ru']:
        new_parser = Parser(domain=parser_domain)
        session.add(new_parser)
    session.commit()

    response = client.get('/api/v1/parsers')
    assert '200' in response.status
    assert len(response.json) > 0


def test_create_new_parser(client):
    parser_data = {'domain': 'site-for-new-parser.com'}
    response = client.post('/api/v1/parsers',
                            data=json.dumps(parser_data),
                            content_type='application/json')

    parser = Parser.query.filter(Parser.domain == parser_data['domain']).one()

    assert (parser.domain in response.json.get('domain') and
            parser.id == response.json.get('id'))


def test_get_parser(parser, client):
    response = client.get('/api/v1/parsers/{}'.format(parser.id))
    assert response.json.get('id') is parser.id
    assert response.json.get('domain') in parser.domain


def test_update_parser(parser, client):
    update_data = {'status': 'processing'}
    client.put('/api/v1/parsers/{}'.format(parser.id),
               data=json.dumps(update_data),
               content_type='application/json')

    assert parser.status in update_data.get('status')

def test_delete_user(parser, client):
    client.delete('/api/v1/parsers/{}'.format(parser.id))
    assert Parser.query.filter_by(domain=parser.domain).first() is None
