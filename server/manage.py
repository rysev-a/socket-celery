from flask_socketio import SocketIO
from flask_script import Manager
from flask_migrate import MigrateCommand
from app import create_app
from app.database import db
manager = Manager(app=create_app('app.settings'))

@manager.command
def init_db():
    db.create_all()

@manager.command
def debug():
    app = create_app('app.settings')
    socketio = SocketIO(app, message_queue=app.config['SOCKETIO_REDIS_URL'])
    socketio.run(app)

@manager.command
def mockup():
    db.create_all()
    from app.parsers.models import Parser
    for domain in ['http://b2b-russia.ru/',
                 'http://bpages.ru/',
                 'http://www.orgpage.ru/']:
        db.session.add(Parser(domain=domain))
    db.session.commit()

if __name__ == '__main__':
    manager.add_command('db', MigrateCommand)
    manager.run()
