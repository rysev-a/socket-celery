#!/usr/bin/env python
import os
from app import create_app
from app.celery import celery_app

app = create_app('app.settings')
app.app_context().push()
