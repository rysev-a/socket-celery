from flask_restful import Resource, fields, marshal
from flask_login import current_user, login_user, logout_user
from flask import request, jsonify

from .models import User
from .forms import UserForm
from ..bcrypt import bcrypt
from ..database import db


role_fields = {
    'id': fields.Integer,
    'name': fields.String
}

user_fields = {
    'id': fields.Integer,
    'email': fields.String,
    'phone': fields.String,
    'first_name': fields.String,
    'last_name': fields.String,
    'is_authenticated': fields.Boolean,
    'roles': fields.List(fields.Nested(role_fields))
}


def require_authorization(func):
    def decorator(*args, **kwargs):
        if not current_user.is_authenticated:
            return {'error': 'authorization required'}, 401

        if kwargs.get('user_id') is not current_user.id:
            return {'error': 'permission denied'}, 403

        return func(*args, **kwargs)
    return decorator


class UserList(Resource):
    def get(self):
        users = User.query.all()
        return marshal(users, user_fields), 200

    def post(self):
        data = request.json
        form = UserForm(**data)

        if not form.validate():
            response = jsonify(form.errors)
            response.status_code = 400
            return response

        user = User(**data)
        db.session.add(user)
        db.session.commit()

        return marshal(user, user_fields), 201


class UserItem(Resource):
    def get(self, user_id):
        user = User.query.get(user_id)
        if not user:
            return {'message': 'not found'}, 400
        return marshal(user, user_fields), 200

    def put(self, user_id):
        user = User.query.filter_by(id=user_id)
        user.update(request.json)
        db.session.commit()
        return marshal(user.first(), user_fields), 200

    def delete(self, user_id):
        User.query.filter_by(id=user_id).delete()
        db.session.commit()
        return {'message': 'delete complete'}, 200


class ProfileLogin(Resource):
    def post(self):
        email = request.get_json()['email']
        password = request.get_json()['password']
        user = User.query.filter_by(email=email).first()

        if not user:
            return {'email': 'invalid email'}, 400

        if(not bcrypt.check_password_hash(user.password, password)):
            return {'password': 'invalid password'}, 400

        login_user(user, remember=True)
        return marshal(current_user, user_fields), 200


class ProfileRegister(Resource):
    def post(self):
        data = request.json
        form = UserForm(**data)

        if not form.validate():
            response = jsonify(form.errors)
            response.status_code = 400
            return response

        user = User(**data)
        db.session.add(user)
        db.session.commit()
        login_user(user, remember=True)

        return marshal(current_user, user_fields), 200


class ProfileLogout(Resource):
    def post(self):
        logout_user()
        return {'message': 'logout success'}, 200    


class ProfileCurrent(Resource):
    def get(self):
        if not current_user.is_authenticated:
            return {'error': 'authorization required'}, 400
        
        return marshal(current_user, user_fields), 200        
