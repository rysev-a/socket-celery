from sqlalchemy.ext.hybrid import hybrid_property
from ..database import db
from ..bcrypt import bcrypt


users_roles = db.Table('users_roles',
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
    db.Column('role_id', db.Integer, db.ForeignKey('roles.id'))
)

class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(length=255),  unique=True)
    phone = db.Column(db.String(length=255))
    first_name = db.Column(db.String(length=255))
    last_name = db.Column(db.String(length=255))

    roles = db.relationship('Role',
        secondary=users_roles,
        backref=db.backref('users', lazy='dynamic'),
        lazy='dynamic')

    _password = db.Column('password', db.String(255))

    @hybrid_property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = bcrypt.generate_password_hash(password).decode("utf-8")

    def __repr__(self):
        return 'User %s' % self.email

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), unique=True)

    def __repr__(self):
        return self.name
