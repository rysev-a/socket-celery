from flask_restful import Api
from .users.resources import (UserList, UserItem, ProfileLogin,
                              ProfileLogout, ProfileCurrent, ProfileRegister)

from .emails.resources import EmailList
from .parsers.resources import (ParserList, ParserItem, ParserStart,
                                ParserStop, ParserGenerateExcel, ParserFromExcel)

from .distributors.resources import (DistributorList, DistributorItem,
                                     DistributorStart)

api = Api()

# User API
api.add_resource(UserList, '/api/v1/users')
api.add_resource(UserItem, '/api/v1/users/<int:user_id>')

# Profile API
api.add_resource(ProfileLogin, '/api/v1/profile/login')
api.add_resource(ProfileLogout, '/api/v1/profile/logout')
api.add_resource(ProfileCurrent, '/api/v1/profile/current')
api.add_resource(ProfileRegister, '/api/v1/profile/register')

# Email API
api.add_resource(EmailList, '/api/v1/emails')

# Parser API
api.add_resource(ParserList, '/api/v1/parsers')
api.add_resource(ParserItem, '/api/v1/parsers/<int:parser_id>')
api.add_resource(ParserStart, '/api/v1/parsers/<int:parser_id>/start')
api.add_resource(ParserStop, '/api/v1/parsers/<int:parser_id>/stop')
api.add_resource(ParserGenerateExcel, '/api/v1/parsers/<int:parser_id>/excel')
api.add_resource(ParserFromExcel, '/api/v1/parsers/upload')

# Distributor API
api.add_resource(DistributorList, '/api/v1/distributors')
api.add_resource(DistributorItem, '/api/v1/distributors/<int:distributor_id>')
api.add_resource(DistributorStart, '/api/v1/distributors/<int:distributor_id>/start')
