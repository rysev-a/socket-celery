from .settings import CELERY_BROKER_URL
from celery import Celery
celery_app = Celery(__name__, broker=CELERY_BROKER_URL)
