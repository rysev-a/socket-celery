SQLALCHEMY_TRACK_MODIFICATIONS = True
SQLALCHEMY_DATABASE_URI = 'postgresql://alex:alex88@localhost:5432/email-parser'
SECRET_KEY = 'asdf00#$sdfsdfsdf'
SOCKETIO_REDIS_URL = 'redis://localhost:6379/0'
CELERY_BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
DEBUG = True
