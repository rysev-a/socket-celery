import math
from flask_restful import Resource, fields, marshal, reqparse
from .models import Email


parser_fields = {
    'id': fields.Integer,
    'domain': fields.String
}

email_fields = {
    'id': fields.Integer,
    'address': fields.String,
    'source_page': fields.String,
    'parser': fields.Nested(parser_fields)
}


class EmailList(Resource):
    def get(self):
        per_page = 15

        email_count = Email.query.count()
        page_count = math.ceil(email_count / per_page)

        parser = reqparse.RequestParser()
        parser.add_argument('page', type=int)
        args = parser.parse_args()
        page = args.get('page')
        if page:
            emails = Email.query.limit(per_page).offset((page - 1) * per_page).all()
        else:
            emails = Email.query.all()
        return {
            'data': marshal(emails, email_fields),
            'page': page,
            'page_count': page_count
        }, 200
