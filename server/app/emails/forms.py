from wtforms import StringField
from wtforms.validators import DataRequired
from wtforms_alchemy.validators import Unique
from wtforms_alchemy import ModelForm
from .models import Email


class EmailForm(ModelForm):
    address = StringField('email', validators=[DataRequired(), 
                                               Unique(Email.address)])
