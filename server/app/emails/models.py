from datetime import datetime
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from ..import db


class Email(db.Model):
    __tablename__ = 'emails'

    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(length=500), unique=True)
    datetime = db.Column(db.DateTime(), default=datetime.utcnow)
    source_page = db.Column(db.String(length=500))
    parser = relationship("Parser", back_populates="emails")
    parser_id = db.Column(db.Integer, ForeignKey('parsers.id', ondelete="CASCADE"))

    def __repr__(self):
        return self.address
