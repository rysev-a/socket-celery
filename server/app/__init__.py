from flask import Flask, render_template
from .database import db
from .bcrypt import bcrypt
from .authorization import login_manager
from .api import api
from .celery import celery_app
from .migrate import migrate
from .mail import mail


def create_app(config):
    app = Flask(__name__)
    app.config.from_object(config)
    db.init_app(app)
    bcrypt.init_app(app)
    login_manager.init_app(app)
    api.init_app(app)
    mail.init_app(app)
    migrate.init_app(app, db)
    celery_app.conf.update(app.config)

    @app.route('/')
    def index():
        return render_template('index.html')


    return app
