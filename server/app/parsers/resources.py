import os
from pyexcelerate import Workbook
from werkzeug import secure_filename
from flask import request, jsonify
from flask_restful import Resource, marshal
from ..database import db
from ..celery import celery_app
from .forms import ParserForm
from .models import Parser
from .tasks import start_parse
from .fields import parser_fields


class ParserList(Resource):
    def get(self):
        parsers = Parser.query.all()
        return marshal(parsers, parser_fields), 200

    def post(self):
        data = request.json
        form = ParserForm(**data)

        if not form.validate():
            response = jsonify(form.errors)
            response.status_code = 400
            return response

        parser = Parser(**data)
        db.session.add(parser)
        db.session.commit()

        return marshal(parser, parser_fields), 201


class ParserItem(Resource):
    def get(self, parser_id):
        parser = Parser.query.get(parser_id)
        if not parser:
            return {'message': 'not found'}, 400
        return marshal(parser, parser_fields), 200

    def put(self, parser_id):
        parser = Parser.query.filter_by(id=parser_id)
        parser.update(request.json)
        db.session.commit()
        return marshal(parser.first(), parser_fields), 200

    def delete(self, parser_id):
        Parser.query.filter_by(id=parser_id).delete()
        db.session.commit()
        return {'message': 'delete complete'}, 200


class ParserStart(Resource):
    def post(self, parser_id):
        start_parse.delay(parser_id)
        return {'id': parser_id, 'status': 'processing'}


class ParserStop(Resource):
    def post(self, parser_id):
        parser = Parser.query.get(parser_id)
        parser.status = 'stopped'

        #stop celery task
        celery_task = parser.celery_task
        celery_app.control.revoke(celery_task, terminate=True)
        
        parser.celery_task = None
        db.session.commit()
        return marshal(parser, parser_fields)

class ParserGenerateExcel(Resource):
    def post(self, parser_id):
        parser = Parser.query.get(parser_id)
        parser.generate()
        return {'excel': '%s.xlsx' % parser.id}

class ParserFromExcel(Resource):
    def post(self):
        file = request.files['file']
        filename = secure_filename(file.filename)
        upload_path = os.path.dirname(__file__) + '/upload'
        file.save(os.path.join(upload_path, filename))

        parser = Parser(domain=filename)
        db.session.add(parser)
        db.session.commit()
        parser.load(os.path.join(upload_path, filename))

        return marshal(parser, parser_fields)
