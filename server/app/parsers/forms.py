from wtforms import StringField
from wtforms.validators import DataRequired
from wtforms_alchemy.validators import Unique
from wtforms_alchemy import ModelForm
from .models import Parser


class ParserForm(ModelForm):
    domain = StringField('domain', 
        validators=[DataRequired(), Unique(Parser.domain)])

