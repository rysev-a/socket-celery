from ..celery import celery_app
from .models import Parser


@celery_app.task(bind=True)
def start_parse(self, parser_id):
    parser = Parser.query.get(parser_id)
    parser.parse(self.request.id)
