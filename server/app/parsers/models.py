import time
import re
import requests
import collections
import xlrd
from pyexcelerate import Workbook
from lxml import html
from urllib import parse as urlparse
from sqlalchemy.orm import relationship
from datetime import datetime
from flask_socketio import SocketIO
from flask_restful import marshal
from ..settings import SOCKETIO_REDIS_URL
from ..database import db
from ..celery import celery_app
from ..emails.models import Email
from ..emails.forms import EmailForm
from .fields import parser_fields


local_socketio = SocketIO(message_queue=SOCKETIO_REDIS_URL,
                          namespace='/socket')

class Parser(db.Model):
    __tablename__ = 'parsers'

    id = db.Column(db.Integer, primary_key=True)
    domain = db.Column(db.String(length=255), unique=True)
    start_datetime = db.Column(db.DateTime())
    end_datetime = db.Column(db.DateTime())
    status = db.Column(db.String(length=10), default='ready')
    celery_task = db.Column(db.String(length=100))
    emails = relationship("Email")

    def parse(self, celery_task):
        self.celery_task = celery_task
        db.session.commit()

        try:
            requests.get(self.domain)
            self.start()
        except:
            self.error()
            return False

        urls_queue = collections.deque()
        urls_queue.append(self.domain)  
        found_urls = set()
        self.found_emails = set()

        while len(urls_queue):
            url = urls_queue.popleft()
            print(url)

            # emit to frontend
            emit_info ={'nowPage': url,
                        'emails': len(self.found_emails)}
            local_socketio.emit('parser#%s:info' % self.id, 
                                 emit_info,
                                 namespace='/socket')

            found_urls.add(url)
            try:
                response = requests.get(url)
            except:
                print('continue')
                local_socketio.emit('parser#%s:info' % self.id, 
                     {'nowPage': 'Страница %ы недоступна' % url},
                     namespace='/socket')
                continue

            parsed_body = html.fromstring(response.content)
            self.get_emails(response.text, url)

            # find all pages
            links = [urlparse.urljoin(response.url, url) for 
                     url in parsed_body.xpath('//a/@href')]

            for link in links:
                # add to queue
                if link not in found_urls and link.startswith(self.domain):
                    urls_queue.append(link)
                    
                
        self.complete()

    def get_emails(self, page_content, source_page):
        template = '[0-9a-z\-]+@[0-9a-z\-]+\.[0-9a-z]+'
        search_result = re.findall(template, page_content, re.IGNORECASE)
        email_list = set(search_result)
        for email in email_list:
            email = email.lower()
            form = EmailForm(address=email)
            if form.validate():
                self.found_emails.add(email)
                new_email = Email(address=email, 
                                  source_page=source_page, 
                                  parser=self)
                db.session.add(new_email)
                db.session.commit()

    def start(self):
        self.status = 'running'
        self.start_datetime = datetime.utcnow()
        self.end_datetime = None
        db.session.commit()
        self.emit()

    def complete(self):
        self.status = 'complete'
        self.end_datetime = datetime.utcnow()
        db.session.commit()
        self.emit()

    def error(self):
        self.status = 'error'
        db.session.commit()
        self.emit()

    def emit(self):
        local_socketio.emit('parsers:update', 
                            marshal(self, parser_fields),
                            namespace='/socket')

    def generate(self):
        excel_emails = [[email.address, 
                         email.source_page, 
                         email.parser.domain] 
                         for email in self.emails]

        wb = Workbook()
        wb.new_sheet("emails", data=excel_emails)
        wb.save("../client/public/%s.xlsx" % self.id)

    def load(self, filename):
        book = xlrd.open_workbook(filename)
        sh = book.sheet_by_index(0)
        for rx in range(sh.nrows):
            source_page = "==========="
            if len(sh.row(rx)) > 1:
                source_page = sh.row(rx)[1].value

            data = dict(address=sh.row(rx)[0].value,
                        source_page=source_page,
                        parser_id=self.id)

            form = EmailForm(**data)
            if form.validate():
                new_email = Email(**data)
                db.session.add(new_email)

        db.session.commit()

    def __repr__(self):
        return 'parse from %s' % self.domain
