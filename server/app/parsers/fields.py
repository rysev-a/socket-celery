from flask_restful import fields


parser_fields = {
    'id': fields.Integer,
    'domain': fields.String,
    'start_datetime': fields.String,
    'end_datetime': fields.String,
    'status': fields.String,
    'celery_task': fields.String
}
