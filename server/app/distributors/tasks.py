from ..celery import celery_app
from .models import Distributor


@celery_app.task(bind=True)
def start_distribute(self, distributor_id):
    distributor = Distributor.query.get(distributor_id)
    distributor.distribute(self.request.id)
