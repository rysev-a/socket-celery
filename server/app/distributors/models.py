import re
import time
import random
from datetime import datetime
from flask_mail import Message
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from flask_socketio import SocketIO
from ..database import db
from ..mail import mail
from ..settings import SOCKETIO_REDIS_URL

local_socketio = SocketIO(message_queue=SOCKETIO_REDIS_URL,
                          namespace='/socket')

class Distributor(db.Model):
    __tablename__ = 'distributors'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(length=500), unique=True)
    sender = db.Column(db.String(length=500))
    template = db.Column(db.String(length=5000))
    subject = db.Column(db.String(length=500))
    start_datetime = db.Column(db.DateTime(), default=datetime.utcnow)
    status = db.Column(db.String(length=10), default='ready')
    parser = relationship("Parser")
    parser_id = db.Column(db.Integer, ForeignKey('parsers.id', ondelete='SET NULL'))

    def render(self):
        letter_text = self.template
        search_result = re.findall('{[^}]+}', letter_text, re.IGNORECASE)

        for result in search_result:
            letter_text = letter_text.replace(result, 
                random.choice(result[1:-1].split('|')))

        return letter_text

    def distribute(self, celery_task):
        self.celery_task = celery_task
        db.session.commit()

        # emit to frontend
        emit_info = {'id': self.id, 'status': 'running'}
        local_socketio.emit('distributor#%s:info' % self.id,
                             emit_info,
                             namespace='/socket')

        for email in self.parser.emails:
            msg = Message(subject=self.subject,
                          body=self.render(),
                          sender=self.sender,
                          recipients=[email.address])
            mail.send(msg)

        self.status = 'complete'

        # emit to frontend
        emit_info = {'id': self.id, 'status': 'complete'}
        local_socketio.emit('distributor#%s:info' % self.id,
                             emit_info,
                             namespace='/socket')

        db.session.commit()

    def __repr__(self):
        return self.address
