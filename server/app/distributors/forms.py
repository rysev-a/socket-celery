from wtforms import StringField, IntegerField
from wtforms.validators import DataRequired
from wtforms_alchemy.validators import Unique
from wtforms_alchemy import ModelForm
from .models import Distributor


class DistributorForm(ModelForm):
    title = StringField('title', validators=[DataRequired(), Unique(Distributor.title)])


