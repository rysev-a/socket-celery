from flask import jsonify, request
from flask_restful import Resource, fields, marshal
from ..database import db
from .models import Distributor
from .forms import DistributorForm
from .tasks import start_distribute


parser_fields = {
    'id': fields.Integer,
    'domain': fields.String
}

distributor_fields = {
    'id': fields.Integer,
    'title': fields.String,
    'sender': fields.String,
    'template': fields.String,
    'status': fields.String,
    'start_datetime': fields.DateTime,
    'subject': fields.String,
    'parser': fields.Nested(parser_fields)
}


class DistributorList(Resource):
    def get(self):
        distributors = Distributor.query.all()
        return marshal(distributors, distributor_fields), 200

    def post(self):
        data = request.json
        form = DistributorForm(**data)

        if not form.validate():
            response = jsonify(form.errors)
            response.status_code = 400
            return response

        distributor = Distributor(**data)
        db.session.add(distributor)
        db.session.commit()

        return marshal(distributor, distributor_fields), 201


class DistributorItem(Resource):
    def get(self, distributor_id):
        distributor = Distributor.query.get(distributor_id)
        if not distributor:
            return {'message': 'not found'}, 400
        return marshal(distributor, distributor_fields), 200

    def put(self, distributor_id):
        distributor = Distributor.query.filter_by(id=distributor_id)
        if request.json.get('parser_id'):
            if int(request.json.get('parser_id')) == 0:
                request.json['parser_id'] = None
    
        distributor.update(request.json)
        db.session.commit()
        return marshal(distributor.first(), distributor_fields), 200

    def delete(self, distributor_id):
        Distributor.query.filter_by(id=distributor_id).delete()
        db.session.commit()
        return {'message': 'delete complete'}, 200


class DistributorStart(Resource):
    def post(self, distributor_id):
        start_distribute.delay(distributor_id)
        return {'id': distributor_id, 'status': 'processing'}
