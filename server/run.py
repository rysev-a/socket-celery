from flask_socketio import SocketIO
from app import create_app


app = create_app('app.settings')
socketio = SocketIO(app, message_queue=app.config['SOCKETIO_REDIS_URL'])

if __name__ == '__main__':
    socketio.run(app)
