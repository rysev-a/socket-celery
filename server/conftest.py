import pytest
import os
from app import create_app
from app.database import db as database
from app.users.models import User
from app.parsers.models import Parser

DB_LOCATION = '/tmp/test_app.db'


@pytest.fixture(scope='session')
def app():
    app = create_app(config='app/test_settings')
    return app


@pytest.fixture(scope='session')
def db(app, request):
    if os.path.exists(DB_LOCATION):
        os.unlink(DB_LOCATION)
    database.app = app
    database.create_all()

    def teardown():
        database.drop_all()
        os.unlink(DB_LOCATION)

    request.addfinalizer(teardown)
    return database


@pytest.fixture(scope='function')
def session(db, request):
    session = db.create_scoped_session()
    db.session = session

    def teardown():
        session.remove()

    request.addfinalizer(teardown)
    return session


@pytest.fixture(scope='function')
def user(session, request):
    data = {'email': 'email@ok.ru', 'password': 'pass'}
    user = User(**data)
    session.add(user)
    session.commit()

    def teardown():
        user.query.delete()
        session.commit()

    request.addfinalizer(teardown)
    return user



@pytest.fixture(scope='function')
def parser(session, request):
    data = {'domain': 'site-for-parse.com'}
    parser = Parser(**data)
    session.add(parser)
    session.commit()

    def teardown():
        parser.query.delete()
        session.commit()

    request.addfinalizer(teardown)
    return parser
