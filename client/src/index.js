import React from 'react';
import {Provider} from 'react-redux';
import {render} from 'react-dom';
import router from 'app/router';
import store from 'app/store';


render(<Provider store={store}>{router}</Provider>,
       document.getElementById('app'));


