import list from './list/constants';
import create from './create/constants';
import item from './item/constants';
import edit from './edit/constants';


export default Object.assign(list, create, item, edit)
