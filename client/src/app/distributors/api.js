import settings from 'settings';
import {serializeEditDistributor} from './serializers';


const {API_URL} = settings;
const baseUrl = `${API_URL}/distributors`;

let api = {
  list: ()=> {
    return fetch(`${baseUrl}`);
  },

  item: (id)=> {
    return fetch(`${baseUrl}/${id}`)
  },

  add: (item) => {
    return fetch(baseUrl, {
      method: 'post',
      body: JSON.stringify(item),
      headers: {'Content-Type': 'application/json'}
    });
  },

  remove: (id)=> {
    return fetch(`${baseUrl}/${id}`, {
      method: 'delete'
    })
  },

  update (item) {
    return fetch(`${baseUrl}/${item.id}`, {
      method: 'put',
      body: JSON.stringify(serializeEditDistributor(item)),
      headers: {'Content-Type': 'application/json'}
    })
  },

  start (id) {
    return fetch(`${baseUrl}/${id}/start`, {
      method: 'post',
      credentials: 'include',
      headers: {'Content-Type': 'application/json'}
    })
  }
};

export default api;
