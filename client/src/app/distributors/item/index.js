import {connect} from 'react-redux';
import view from './view';
import actions from './actions';

let mapStateToProps = (state) => {
  return {
    distributor: state.distributors.item
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    fetch: (id) => dispatch(actions.fetch(id)),
    update: (data) => dispatch(actions.update(data)),
    reset: () => dispatch(actions.reset()),
    start: (id)=> dispatch(actions.start(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(view)
