import constants from 'app/constants';
import api from '../api';

let actions = {
  fetch: (id)=> {
    return (dispatch)=> {
      dispatch({type: constants.FETCH_DISTRIBUTOR_ITEM});
      api.item(id)
        .then((response) => response.json())
        .then((json) => {
          dispatch({
            type: constants.FETCH_DISTRIBUTOR_ITEM_SUCCESS,
            payload: json
          });
        })
    }
  },

  update: (data)=> {
    return {
      type: constants.UPDATE_DISTRIBUTOR_ITEM,
      payload: data
    };
  },

  reset: ()=> {
    return {
      type: constants.RESET_DISTRIBUTOR_ITEM
    }
  },

  start: (id)=> {
    return (dispatch)=> {
      dispatch({type: constants.START_DISTRIBUTOR_ITEM});
      api.start(id)
        .then((response) => response.json())
        .then((json) => {
          dispatch({
            type: constants.START_DISTRIBUTOR_ITEM_SUCCESS,
            payload: json
          });
        })
    }
  }
};


export default actions
