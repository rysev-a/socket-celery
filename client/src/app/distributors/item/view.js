import React from 'react';
import {Component} from 'react';
import socket from 'app/socket';
import {translateStatus} from '../utils';

class DistributorItemView extends Component {
  componentDidMount () {
    this.props.fetch(this.props.params.id);
    socket.on(`distributor#${this.props.params.id}:info`, (info)=> {
      console.log(info);
      this.props.update(info);
    });
  }

  componentWillUnmount () {
    socket.removeListener(`distributor#${this.props.params.id}:info`);
    this.props.reset();
  }

  render () {
    if (this.props.distributor.status != 'complete') {
      return <div>...loading</div>;
    }

    let distributor = this.props.distributor.data;
    return <div className="distributor-item">
      <div className="distributor-item__title">Название</div>
      <div className="distributor-item__value">
        {distributor.title}
      </div>

      <div className="distributor-item__title">Парсер</div>
      <div className="distributor-item__value">
        {distributor.parser.domain}
      </div>

      <div className="distributor-item__title">Отправитель</div>
      <div className="distributor-item__value">
        {distributor.sender}
      </div>

      <div className="distributor-item__title">Тема</div>
      <div className="distributor-item__value">
        {distributor.subject}
      </div>

      <div className="distributor-item__title">Шаблон</div>
      <div className="distributor-item__value">
        {distributor.template}
      </div>

      <div className="distributor-item__title">Статус</div>
      <div className="distributor-item__value">
        {translateStatus(distributor.status)}
      </div>

      <div className="distributor-item__buttons">
        <a className="button button-primary"
           onClick={this.props.start.bind(null, distributor.id)}>Запустить</a>
      </div>
    </div>;
  }
}

export default DistributorItemView;
