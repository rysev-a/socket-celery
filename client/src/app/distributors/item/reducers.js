import constants from 'app/constants';
import {combineReducers} from 'redux';


let defaultState = () => {
  return {
    title: '',
    sender: '',
    template: ''
  };
};

function data (state = defaultState(), action) {
  switch (action.type) {
    case constants.FETCH_DISTRIBUTOR_ITEM_SUCCESS:
      return Object.assign({}, state, action.payload);

    case constants.UPDATE_DISTRIBUTOR_ITEM:
      return Object.assign({}, state, action.payload);

    case constants.RESET_DISTRIBUTOR_ITEM:
      return defaultState();

    case constants.START_DISTRIBUTOR_ITEM_SUCCESS:
      return Object.assign({}, state, action.payload);

    default:
      return state;
  }
}

function status (state = 'loading', action) {
  switch (action.type) {
    case constants.FETCH_DISTRIBUTOR_ITEM_SUCCESS:
      return 'complete';

    case constants.RESET_DISTRIBUTOR_ITEM:
      return 'loading';

    default:
      return state;
  }
}

export default combineReducers({data, status})
