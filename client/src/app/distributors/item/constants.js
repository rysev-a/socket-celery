export default {
  FETCH_DISTRIBUTOR_ITEM: 'fetch distributor item',
  FETCH_DISTRIBUTOR_ITEM_SUCCESS: 'fetch distributor item success',
  UPDATE_DISTRIBUTOR_ITEM: 'update distributor item',
  RESET_DISTRIBUTOR_ITEM: 'reset distributor item',

  START_DISTRIBUTOR_ITEM: 'start distributor item',
  START_DISTRIBUTOR_ITEM_SUCCESS: 'start distributor item success'
}
