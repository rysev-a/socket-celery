import {connect} from 'react-redux';
import view from './view';
import actions from './actions';


let mapStateToProps = (state) => {
  return {
    distributors: state.distributors.list
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    fetch: () => dispatch(actions.fetch()),
    remove: (id) => dispatch(actions.remove(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(view)
