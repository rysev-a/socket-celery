import constants from 'app/constants';
import api from '../api';

let actions = {
  fetch: ()=> {
    return (dispatch)=> {
      dispatch({type: constants.FETCH_DISTRIBUTOR_LIST});
      api.list()
        .then((response) => response.json())
        .then((json) => {
          dispatch({
            type: constants.FETCH_DISTRIBUTOR_LIST_SUCCESS,
            payload: json
          });
        })
    }
  },

  remove: (id)=> {
    return (dispatch)=> {
      dispatch({type: constants.REMOVE_DISTRIBUTOR});
      api.remove(id)
        .then((response) => {
          if (response.ok) {
            dispatch({
              type: constants.REMOVE_DISTRIBUTOR_SUCCESS,
              payload: id
            });
          }
        })
    }
  },
};


export default actions
