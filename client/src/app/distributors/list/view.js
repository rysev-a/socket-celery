import React from 'react';
import {Component} from 'react';
import {Link} from 'react-router';
import {translateStatus} from '../utils';
import Moment from 'moment';


class DistributorListView extends Component {

  render() {
    return <div className="distributor-list">
      <table className="u-full-width">
        <thead>
          <tr>
            <th>Название</th>
            <th>Статус</th>
            <th>Парсер</th>
            <th>Редактировать</th>
            <th>Удалить</th>
          </tr>
        </thead>
        <tbody>
          {this.showDistributors()}
        </tbody>
      </table>
      <div className="distributor-list__buttons">
        <Link className="button button-primary"
              to="/distributors/new">Создать</Link>
      </div>
    </div>;
  }

  showDistributors () {
    if (this.props.distributors.data.length) {
      return this.props.distributors.data.map(this.distributorItem.bind(this));
    }

    return <tr><td colSpan='2'>-----</td></tr>
  }

  distributorItem (distributor) {
    return <tr key={distributor.id}>
      <td>
        <Link to={`distributors/${distributor.id}`}>{distributor.title}</Link>
      </td>
      <td>{translateStatus(distributor.status)}</td>
      <td>{distributor.parser.domain}</td>
      <td>
        <Link to={`distributors/${distributor.id}/edit`}
              className="button button-primary">Редактировать</Link>
      </td>
      <td><a className="button button-primary"
             onClick={this.props.remove.bind(null, distributor.id)}>Удалить</a>
      </td>
    </tr>
  }

  componentDidMount() {
    if (this.props.distributors.data.length == 0) {
      this.props.fetch();
    }
  }

}

export default DistributorListView;
