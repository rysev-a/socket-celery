import Immutable from 'immutable';
import {combineReducers} from 'redux';
import constants from 'app/constants';

function data (state = [], action) {
  switch (action.type) {
    case constants.FETCH_DISTRIBUTOR_LIST_SUCCESS:
      return action.payload;

    case constants.REMOVE_DISTRIBUTOR_SUCCESS:
      let removedIndex = state.indexOf(state.filter((t)=> t.id == action.payload)[0]);
      return Immutable.fromJS(state).delete(removedIndex).toJS();

    case constants.ADD_DISTRIBUTOR_CREATE_SUCCESS:
      return Immutable.fromJS(state).push(action.payload).toJS();

    case constants.SAVE_DISTRIBUTOR_EDIT_SUCCESS:
      let savedIndex = state.indexOf(state.filter((t)=> t.id == action.payload.id)[0]);
      return Immutable.fromJS(state).set(savedIndex, action.payload).toJS();

    default:
      return state;
  }
}

export default combineReducers({data})
