export default {
  FETCH_DISTRIBUTOR_LIST: 'fetch distributor list',
  FETCH_DISTRIBUTOR_LIST_SUCCESS: 'fetch distributor list success',
  REMOVE_DISTRIBUTOR: 'remove ditributor',
  REMOVE_DISTRIBUTOR_SUCCESS: 'remove distributor success'
}
