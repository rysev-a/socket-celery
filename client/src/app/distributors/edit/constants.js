export default {
  FETCH_DISTRIBUTOR_EDIT: 'fetch distributor edit',
  FETCH_DISTRIBUTOR_EDIT_SUCCESS: 'fetch distributor edit success',

  UPDATE_DISTRIBUTOR_EDIT: 'update distributor edit',
  RESET_DISTRIBUTOR_EDIT: 'reset distributor edit',

  SAVE_DISTRIBUTOR_EDIT: 'save distributor edit',
  SAVE_DISTRIBUTOR_EDIT_SUCCESS: 'save distributor edit success',
  SAVE_DISTRIBUTOR_EDIT_ERROR: 'save distributor edit error'
}
