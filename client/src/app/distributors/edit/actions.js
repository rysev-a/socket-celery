import {browserHistory} from 'react-router';
import constants from 'app/constants';
import api from '../api';

let actions = {
  fetch: (id)=> {
    return (dispatch)=> {
      dispatch({type: constants.FETCH_DISTRIBUTOR_EDIT});
      api.item(id)
        .then((response) => response.json())
        .then((json) => {
          dispatch({
            type: constants.FETCH_DISTRIBUTOR_EDIT_SUCCESS,
            payload: json
          });
        })
    }
  },

  save: (item)=> {
    return (dispatch)=> {
      dispatch({type: constants.SAVE_DISTRIBUTOR_EDIT});
      
      //initialize requestDispatchType variable for later use
      let requestDispatchType;
      api.update(item)
        .then((response) => {
          //set requestDispatchType variable
          requestDispatchType = response.ok ?
            constants.SAVE_DISTRIBUTOR_EDIT_SUCCESS : constants.SAVE_DISTRIBUTOR_EDIT_ERROR;
          return response.json()
        })
        .then((json) => {
          dispatch({
            type:    requestDispatchType,
            payload: json
          });
          browserHistory.push('/distributors');
        })
    }    
  },

  update (field, value) {
    return (dispatch)=> {
      dispatch({
        type: constants.UPDATE_DISTRIBUTOR_EDIT,
        payload: {field, value}
      })
    }
  },

  reset: ()=> {
    return {
      type: constants.RESET_DISTRIBUTOR_EDIT
    }
  }
};


export default actions
