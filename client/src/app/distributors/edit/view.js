import React from 'react';
import {Component} from 'react';
import {translateStatus} from '../utils';

class DistributorEditView extends Component {
  componentDidMount () {
    // load parsers if need
    if (this.props.parsers.length == 0) {
      this.props.fetchParsers();
    }

    this.props.fetch(this.props.params.id);
  }

  componentWillUnmount () {
    this.props.reset();
  }

  render () {
    if (this.props.distributor.status != 'complete') {
      return <div>...loading</div>;
    }

    let distributor = this.props.distributor.data;

    return <div className="distributor-create">
      <label>Название</label>
      <input type="text"
             className={this.getClassName('title')}
             placeholder="Введите название"
             defaultValue={distributor.title}
             onChange={this.update.bind(this, 'title')}/>

      <label>Парсер</label>
      <select onChange={this.update.bind(this, 'parser_id')}
              defaultValue={distributor.parser.id}
              className={this.getClassName('parser_id')}>
        <option value="0">Выберите парсер</option>
        {this.parserOption()}
      </select>

      <label>От кого</label>
      <input type="text"
             className={this.getClassName('sender')}
             placeholder="Оптравитель"
             defaultValue={distributor.sender}
             onChange={this.update.bind(this, 'sender')}/>

      <label>Тема письма</label>
      <input type="text"
             className={this.getClassName('subject')}
             placeholder="Оптравитель"
             defaultValue={distributor.subject}
             onChange={this.update.bind(this, 'subject')}/>

      <label>Шаблон письма</label>
      <textarea className={this.getClassName('template')}
                placeholder="Содержимое"
                defaultValue={distributor.template}
                onChange={this.update.bind(this, 'template')}/>

      <div className="distributor-edit__buttons">
        <a className="button button-primary"
           onClick={this.save.bind(this)}>Сохранить</a>
      </div>

    </div>;
  }

  getClassName (field) {
    if (this.props.distributor.errors[`${field}`]) {
      return 'field-error';
    }

    return 'field';
  }

  update (field, e) {
    this.props.update(field, e.target.value);
  }

  save () {
    this.props.save(this.props.distributor.data);
  }

  parserOption () {
    if (this.props.parsers.length == 0) {
      return <option>loading...</option>
    }

    return this.props.parsers.map((parser)=> {
      return <option key={parser.id} value={parser.id}>{parser.domain}</option>;
    });
  }
}

export default DistributorEditView;
