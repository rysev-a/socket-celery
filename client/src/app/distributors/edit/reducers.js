import constants from 'app/constants';
import {combineReducers} from 'redux';


let defaultState = () => {
  return {
    title: '',
    sender: '',
    template: ''
  };
};

function data (state = defaultState(), action) {
  switch (action.type) {
    case constants.FETCH_DISTRIBUTOR_EDIT_SUCCESS:
      return Object.assign({}, state, action.payload);

    case constants.UPDATE_DISTRIBUTOR_EDIT:
      let {field, value} = action.payload;
      return Object.assign({}, state, {[`${field}`]: value});

    case constants.RESET_DISTRIBUTOR_EDIT:
      return defaultState();

    default:
      return state;
  }
}

function status (state = 'loading', action) {
  switch (action.type) {
    case constants.FETCH_DISTRIBUTOR_EDIT_SUCCESS:
      return 'complete';

    case constants.RESET_DISTRIBUTOR_EDIT:
      return 'loading';

    default:
      return state;
  }
}

function errors (state = {}, action) {
  switch (action.type) {
    case constants.SAVE_DISTRIBUTOR_EDIT_ERROR:
      return action.payload;

    case constants.UPDATE_DISTRIBUTOR_EDIT:
      let {field} = action.payload;
      return Object.assign({}, state, {[`${field}`]: null});

    case constants.INITIALIZE_DISTRIBUTOR_EDIT:
      return {};

    case constants.RESET_DISTRIBUTOR_EDIT:
      return {};

    default:
      return state;
  }
}


export default combineReducers({data, status, errors})
