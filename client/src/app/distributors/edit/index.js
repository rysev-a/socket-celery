import {connect} from 'react-redux';
import view from './view';
import actions from './actions';
import parserListActions from 'app/parsers/list/actions';


let mapStateToProps = (state) => {
  return {
    distributor: state.distributors.edit,
    parsers: state.parsers.list.data
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    fetch: (id) => dispatch(actions.fetch(id)),
    save: (data) => dispatch(actions.save(data)),
    update: (field, value) => dispatch(actions.update(field, value)),
    reset: () => dispatch(actions.reset()),

    fetchParsers: ()=> {
      dispatch(parserListActions.fetch());
    },
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(view)
