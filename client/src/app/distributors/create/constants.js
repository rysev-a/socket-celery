export default {
  INITIALIZE_DISTRIBUTOR_CREATE: 'initialize distributor create',
  UPDATE_DISTRIBUTOR_CREATE: 'update distributor create',
  RESET_DISTRIBUTOR_CREATE: 'reset distributor create',

  ADD_DISTRIBUTOR_CREATE: 'add distributor create',
  ADD_DISTRIBUTOR_CREATE_SUCCESS: 'add distributor create success',
  ADD_DISTRIBUTOR_CREATE_ERROR: 'add distributor create error'
}
