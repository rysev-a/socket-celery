import constants from 'app/constants';
import {combineReducers} from 'redux';


let defaultState = () => {
  return {
    title: ''
  };
};

function data (state = defaultState(), action) {
  switch (action.type) {
    case constants.UPDATE_DISTRIBUTOR_CREATE:
      let {field, value} = action.payload;
      return Object.assign({}, state, {[`${field}`]: value});

    case constants.INITIALIZE_DISTRIBUTOR_CREATE:
      return defaultState();

    case constants.RESET_DISTRIBUTOR_CREATE:
      return defaultState();

    default:
      return state;
  }
}

function errors (state = {}, action) {
  switch (action.type) {
    case constants.ADD_DISTRIBUTOR_CREATE_ERROR:
      return action.payload;

    case constants.UPDATE_DISTRIBUTOR_CREATE:
      let {field} = action.payload;
      return Object.assign({}, state, {[`${field}`]: null});

    case constants.INITIALIZE_DISTRIBUTOR_CREATE:
      return {};

    case constants.RESET_DISTRIBUTOR_CREATE:
      return {};


    default:
      return state;
  }
}

export default combineReducers({data, errors})
