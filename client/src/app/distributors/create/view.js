import React from 'react';
import {Component} from 'react';


class DistributorCreateView extends Component {
  componentDidMount () {
    // load parsers if need
    if (this.props.parsers.length == 0) {
      this.props.fetchParsers();
    }
  }

  componentWillUnmount () {
    this.props.reset();
  }

  render() {
    return <div className="distributor-create">
      <label>Название</label>
      <input type="text"
             className={this.getClassName('title')}
             placeholder="Введите название"
             onChange={this.update.bind(this, 'title')}/>

      <label>Парсер</label>
      <select onChange={this.update.bind(this, 'parser_id')}
              className={this.getClassName('parser_id')}>
        <option>Выбрать парсер</option>
        {this.parserOption()}
      </select>

      <label>От кого</label>
      <input type="text"
             className={this.getClassName('sender')}
             placeholder="Оптравитель"
             onChange={this.update.bind(this, 'sender')}/>


      <label>Тема письма</label>
      <input type="text"
             className={this.getClassName('subject')}
             placeholder="Оптравитель"
             onChange={this.update.bind(this, 'subject')}/>

      <label>Шаблон письма</label>
      <textarea className={this.getClassName('template')}
                placeholder="Содержимое"
                onChange={this.update.bind(this, 'template')}/>

      <div className="distributor-create__buttons">
        <a className="button button-primary"
           onClick={this.add.bind(this)}>Создать</a>
      </div>
    </div>;
  }

  parserOption () {
    if (this.props.parsers.length == 0) {
      return <option>loading...</option>
    }

    return this.props.parsers.map((parser)=> {
      return <option key={parser.id} value={parser.id}>{parser.domain}</option>;
    });
  }

  update (field, e) {
    this.props.update(field, e.target.value);
  }

  add () {
    let data = this.props.distributor.data;
    this.props.add(data);
  }

  getClassName (field) {
    if (this.props.distributor.errors[`${field}`]) {
      return 'field-error';
    }

    return 'field';
  }
}

export default DistributorCreateView;
