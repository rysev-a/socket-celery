import {connect} from 'react-redux';
import view from './view';
import actions from './actions';
import parserListActions from 'app/parsers/list/actions';


let mapStateToProps = (state) => {
  return {
    distributor: state.distributors.create,
    parsers: state.parsers.list.data
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    update: (field, value)=> {
      dispatch(actions.update(field, value));
    },
    add: (data)=> {
      dispatch(actions.add(data));
    },
    fetchParsers: ()=> {
      dispatch(parserListActions.fetch());
    },
    reset: ()=> {
      dispatch(actions.reset());
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(view)
