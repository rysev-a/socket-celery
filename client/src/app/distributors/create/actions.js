import constants from 'app/constants';
import api from '../api';
import {browserHistory} from 'react-router';


let actions = {
  initialize: ()=> {
    return (dispatch) => {
      dispatch({type: constants.INITIALIZE_DISTRIBUTOR_CREATE})
    }
  },

  update (field, value) {
    return (dispatch)=> {
      dispatch({
        type: constants.UPDATE_DISTRIBUTOR_CREATE,
        payload: {field, value}
      })
    }
  },

  add (data) {
    return (dispatch)=> {
      dispatch({type: constants.ADD_DISTRIBUTOR_CREATE});

      //initialize requestDispatchType variable for later use
      let requestDispatchType;
      api.add(data)
        .then((response) => {
          //set requestDispatchType variable
          requestDispatchType = response.ok ?
            constants.ADD_DISTRIBUTOR_CREATE_SUCCESS : constants.ADD_DISTRIBUTOR_CREATE_ERROR;
          return response.json()
        })
        .then((json) => {
          dispatch({
            type:    requestDispatchType,
            payload: json
          });

          if (requestDispatchType == constants.ADD_DISTRIBUTOR_CREATE_SUCCESS) {
            dispatch({type: constants.RESET_DISTRIBUTOR_CREATE});
            browserHistory.push('/distributors');
          }
        })
    }
  },

  reset () {
    return (dispatch)=> {
      dispatch({type: constants.RESET_DISTRIBUTOR_CREATE});
    }
  }
};


export default actions
