export function serializeEditDistributor (distributor) {
  return {
    'title': distributor.title,
    'sender': distributor.sender,
    'template': distributor.template,
    'subject': distributor.subject,
    'parser_id': distributor.parser_id
  }
}
