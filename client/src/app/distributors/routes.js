import React from 'react';
import {IndexRedirect, IndexRoute, Route} from 'react-router';
import DistributorBaseView from './view';
import DistributorList from './list';
import DistributorCreate from './create';
import DistributorItem from './item';
import DistributorEdit from './edit';


const routes =
  <Route path="distributors" component={DistributorBaseView}>
    <IndexRedirect to='/distributors'/>
    <IndexRoute component={DistributorList}/>
    <Route path='/distributors/new'  component={DistributorCreate} />
    <Route path='/distributors/:id' component={DistributorItem} />
    <Route path='/distributors/:id/edit' component={DistributorEdit} />
  </Route>;


export default routes;
