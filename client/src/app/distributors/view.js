import React from 'react';
import {Component} from 'react';


class DistributorBaseView extends Component {
  render() {
    return <div className="parsers">
      {this.props.children}
    </div>;
  }
}

export default DistributorBaseView;
