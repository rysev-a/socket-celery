import {combineReducers} from 'redux';
import constants from 'app/constants';
import parsers from 'app/parsers/reducers';
import emails from 'app/emails/reducers';
import distributors from 'app/distributors/reducers';


let defaultState = ()=> {
  return {
    processing: true,
    initialized: false
  }
};

function app (state = defaultState(), action) {
  switch (action.type) {
    case constants.INIT_APP:
      return Object.assign({}, state, {processing: false, initialized: true});

    default:
      return state;
  }
}


export default combineReducers({app, parsers, emails, distributors})
