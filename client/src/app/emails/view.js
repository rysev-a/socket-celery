import React from 'react';
import {Component} from 'react';
import {Link} from 'react-router';


class EmailBaseView extends Component {
  render() {
    return <div className="emails">
      {this.props.children}
    </div>;
  }
}

export default EmailBaseView;
