import React from 'react';
import {IndexRedirect, IndexRoute, Route} from 'react-router';
import EmailBaseView from './view';
import EmailList from './list';



const routes =
  <Route path="emails" component={EmailBaseView}>
    <IndexRedirect to='/emails'/>
    <IndexRoute component={EmailList}/>
  </Route>;


export default routes;
