import settings from 'settings';


const {API_URL} = settings;
const baseUrl = `${API_URL}/emails`;

let api = {
  list: (page)=> {
    return fetch(`${baseUrl}?page=${page}`);
  },
};

export default api;
