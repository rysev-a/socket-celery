import Immutable from 'immutable';
import {combineReducers} from 'redux';
import constants from 'app/constants';

function data (state = [], action) {
  switch (action.type) {
    case constants.FETCH_EMAIL_LIST_SUCCESS:
      return action.payload.data;

    case constants.CLEAR_EMAIL_LIST:
    return [];

    default:
      return state;
  }
}


function status(state={page: 1}, action) {
  switch (action.type) {
    case constants.FETCH_EMAIL_LIST_SUCCESS:
      return {
        page: action.payload.page,
        page_count: action.payload.page_count
      };

    case constants.CLEAR_EMAIL_LIST:
    return {page: 1};

    default:
      return state;
  }
}

export default combineReducers({data, status})
