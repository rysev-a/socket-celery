import {connect} from 'react-redux';
import view from './view';
import actions from './actions';


let mapStateToProps = (state) => {
  return {
    emails: state.emails.list
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    fetch: (page) => dispatch(actions.fetch(page)),
    clear: () => dispatch(actions.clear())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(view)
