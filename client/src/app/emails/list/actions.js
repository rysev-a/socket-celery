import constants from 'app/constants';
import api from '../api';

let actions = { 
  fetch: (page)=> {
    return (dispatch)=> {
      dispatch({type: constants.FETCH_EMAIL_LIST});
      api.list(page)
        .then((response) => response.json())
        .then((json) => {
          dispatch({
            type: constants.FETCH_EMAIL_LIST_SUCCESS,
            payload: json
          });
        })
    }
  },

  clear: ()=> {
    return {
      type: constants.CLEAR_EMAIL_LIST
    }
  }
};


export default actions
