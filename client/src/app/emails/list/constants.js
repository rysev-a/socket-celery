export default {
  FETCH_EMAIL_LIST: 'fetch email list',
  FETCH_EMAIL_LIST_SUCCESS: 'fetch email list success',
  CLEAR_EMAIL_LIST: 'clear email list'
}
