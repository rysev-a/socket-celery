import React from 'react';
import {Component} from 'react';


class EmailListView extends Component {
  render () {
    return <div>
      <div className="emails-table-container">
        <table className="emails-table">
          <thead>
            <tr>
              <th>email</th>
              <th>source page</th>
            </tr>
          </thead>
          <tbody>
            {this.props.emails.data.map(this.emailView.bind(this))}
          </tbody>
        </table>
      </div>
      {this.pagination()}
    </div>;
  }

  emailView (email) {
    return <tr key={email.id}>
      <td>{email.address}</td>
      <td>{email.source_page}</td>
    </tr>;
  }

  componentDidMount () {
    this.props.fetch(1);
  }

  componentWillUnmount() {
    this.props.clear();
  }

  pagination () {
    let {page, page_count} = this.props.emails.status;
    let view = [];
    for(let i=1; i <= page_count; i++) {
      view.push(<a key={i}
        onClick={this.props.fetch.bind(null, i)}
        className={this.getPaginationClassName(i, page)}>{i}
      </a>);
    }
    return <div className='pagination'>{view}</div>;
  }

  getPaginationClassName (i, page) {
    if (i == page) {
      return 'pagination__item active';
    }
    return 'pagination__item';
  }
}

export default EmailListView;
