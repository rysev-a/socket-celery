import parsers from './parsers/constants';
import emails from './emails/constants';
import distributors from './distributors/constants';

const appConstants = {
  INIT_APP: 'init app'
};

export default Object.assign(
  appConstants,
  parsers,
  emails,
  distributors
);

