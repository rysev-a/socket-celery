import React from 'react';
import {Component} from 'react';
import {render} from 'react-dom';
import {IndexRoute, Router, Route, browserHistory} from 'react-router';
import {AppView} from './view';
import {StartView} from './view';
import Parsers from './parsers/routes';
import Emails from './emails/routes';
import Distributors from './distributors/routes';

const router =
  <Router history={browserHistory}>
    <Route path="/" component={AppView}>
      <IndexRoute component={StartView}/>
      {Parsers}
      {Emails}
      {Distributors}
    </Route>
  </Router>;

export default router;
