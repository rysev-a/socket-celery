//connect to socket
import io from 'socket.io-client';
export default io.connect('http://' + document.domain + ':' + location.port + '/socket');
