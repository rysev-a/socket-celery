import {combineReducers} from 'redux';
import create from './create/reducers';
import list from './list/reducers';
import item from './item/reducers';


export default combineReducers({create, list, item})
