import React from 'react';
import {IndexRedirect, IndexRoute, Route} from 'react-router';
import ParserBaseView from './view';
import ParserList from './list';
import ParserCreate from './create';
import ParserItem from './item';


const routes =
  <Route path="parsers" component={ParserBaseView}>
    <IndexRedirect to='/parsers'/>
    <IndexRoute component={ParserList}/>
    <Route path='/parsers/new'  component={ParserCreate} />
    <Route path='/parsers/:id' component={ParserItem} />
  </Route>;


export default routes;
