export default {
  FETCH_PARSER_ITEM: 'fetch parser item',
  FETCH_PARSER_ITEM_SUCCESS: 'fetch parser item success',
  UPDATE_PARSER_ITEM: 'update parser item',
  RESET_PARSER_ITEM: 'reset parser item',
  EXCEL_PARSER_ITEM: 'excel parser item',
  EXCEL_PARSER_ITEM_SUCCESS: 'excel parser item success'
}
