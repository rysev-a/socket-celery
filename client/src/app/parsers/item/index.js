import {connect} from 'react-redux';
import view from './view';
import actions from './actions';

let mapStateToProps = (state) => {
  return {
    parser: state.parsers.item
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    fetch: (id) => dispatch(actions.fetch(id)),
    excel: (id) => dispatch(actions.excel(id)),
    update: (data) => dispatch(actions.update(data)),
    reset: () => dispatch(actions.reset())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(view)
