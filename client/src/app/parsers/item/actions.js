import constants from 'app/constants';
import api from '../api';

let actions = { 
  fetch: (id)=> {
    return (dispatch)=> {
      dispatch({type: constants.FETCH_PARSER_ITEM});
      api.item(id)
        .then((response) => response.json())
        .then((json) => {
          dispatch({
            type: constants.FETCH_PARSER_ITEM_SUCCESS,
            payload: json
          });
        })
    }
  },

  update: (data)=> {
    return {
      type: constants.UPDATE_PARSER_ITEM,
      payload: data
    };
  },

  excel: (id)=> {
    return (dispatch)=> {
      dispatch({type: constants.EXCEL_PARSER_ITEM});
      api.excel(id)
        .then((response) => response.json())
        .then((json) => {
          dispatch({
            type: constants.EXCEL_PARSER_ITEM_SUCCESS,
            payload: json
          });
        })
    }
  },

  reset: ()=> {
    return {
      type: constants.RESET_PARSER_ITEM
    }
  }
};


export default actions
