import constants from 'app/constants';
import {combineReducers} from 'redux';


let defaultState = () => {
  return {
    domain: '',
    nowPage: '',
    emails: '',
    excel: ''
  };
};

function data (state = defaultState(), action) {
  switch (action.type) {
    case constants.FETCH_PARSER_ITEM_SUCCESS:
      return Object.assign({}, state, action.payload);

    case constants.EXCEL_PARSER_ITEM_SUCCESS:
      return Object.assign({}, state, action.payload);

    case constants.UPDATE_PARSER_ITEM:
      return Object.assign({}, state, action.payload);

    case constants.STOP_PARSER_SUCCESS:
      if (action.payload.id == state.id) {
        return Object.assign({}, state, {nowPage: '', emails: ''});
      }

      return state;

    case constants.RESET_PARSER_ITEM:
      return defaultState();

    default:
      return state;
  }
}

export default combineReducers({data})
