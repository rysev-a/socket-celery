import React from 'react';
import {Component} from 'react';
import socket from 'app/socket';
import {translateStatus} from '../utils';

class ParserItemView extends Component {
  componentDidMount () {
    this.props.fetch(this.props.params.id);
    socket.on(`parser#${this.props.params.id}:info`, (info)=> {
      this.props.update(info);
    });
  }

  componentWillUnmount () {
    socket.removeListener(`parser#${this.props.params.id}:info`);
    this.props.reset();
  }

  render () {
    let parser = this.props.parser.data;
    return <div className="parsers-item">
      <div className="parsers-item__domain">
        Домен: {parser.domain}
      </div>
      <div className="parsers-item__status">
        Статус: {translateStatus(parser.status)}
      </div>
      <div className="parsers-item__info">
        {this.showInfo()}
      </div>

      <div className="parsers-item__emails">
        {this.showEmails()}
      </div>
      <div className="parsers-item__excel">
        {this.showExcel()}
      </div>
    </div>;
  }

  showInfo () {
    if (this.props.parser.data.nowPage) {   
      return <div>
        Сканируется: {this.props.parser.data.nowPage}
      </div>
    }
  }

  showEmails () {
    if (this.props.parser.data.emails) {   
      return <div>
        Найдено новых email: {this.props.parser.data.emails}
      </div>
    }
  }

  showExcel () {
    let id = this.props.parser.data.id;
    
    if (this.props.parser.data.excel) {
      return <a href={`/${id}.xlsx`}>Скачать</a>;
    }

    return <div>
      <a className="button button-primary"
         onClick={this.props.excel.bind(null, id)}>Получить excel</a>
    </div>
  }
}

export default ParserItemView;
