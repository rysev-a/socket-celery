export default {
  INITIALIZE_PARSER_CREATE: 'initialize parser create',
  UPDATE_PARSER_CREATE: 'update parser create',
  RESET_PARSER_CREATE: 'reset parser create',

  ADD_PARSER_CREATE: 'add parser create',
  ADD_PARSER_CREATE_SUCCESS: 'add parser create success',
  ADD_PARSER_CREATE_ERROR: 'add parser create error'
}
