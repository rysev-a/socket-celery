import constants from 'app/constants';
import {combineReducers} from 'redux';


let defaultState = () => {
  return {
    domain: ''
  };
};

function data (state = defaultState(), action) {
  switch (action.type) {
    case constants.UPDATE_PARSER_CREATE:
      let {field, value} = action.payload;
      return Object.assign({}, state, {[`${field}`]: value});

    case constants.INITIALIZE_PARSER_CREATE:
      return defaultState();

    case constants.RESET_PARSER_CREATE:
      return defaultState();

    default:
      return state;
  }
}

function errors (state = {}, action) {
  switch (action.type) {
    case constants.ADD_PARSER_CREATE_ERROR:
      return action.payload;

    case constants.UPDATE_PARSER_CREATE:
      let {field} = action.payload;
      return Object.assign({}, state, {[`${field}`]: null});

    default:
      return state;
  }
}

export default combineReducers({data, errors})
