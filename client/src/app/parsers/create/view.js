import React from 'react';
import {Component} from 'react';


class ParserCreateView extends Component {
  render() {
    return <div className="parsers-create">
      <input type="text"
             className={this.getClassName('domain')}
             placeholder="Введите сайт"
             onChange={this.update.bind(this, 'domain')}/>
      <div className="parsers-create__buttons">
        <a className="button button-primary"
           onClick={this.add.bind(this)}>Создать</a>
      </div>
    </div>;
  }

  update (field, e) {
    this.props.update(field, e.target.value);
  }

  add () {
    let data = this.props.parser.data;
    this.props.add(data);
  }

  getClassName (field) {
    if (this.props.parser.errors[`${field}`]) {
      return 'field-error';
    }

    return 'field';
  }
}

export default ParserCreateView;
