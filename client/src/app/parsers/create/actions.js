import constants from 'app/constants';
import api from '../api';
import {browserHistory} from 'react-router';


let actions = {
  initialize: ()=> {
    return (dispatch) => {
      dispatch({type: constants.INITIALIZE_PARSER_CREATE})
    }
  },

  update (field, value) {
    return (dispatch)=> {
      dispatch({
        type: constants.UPDATE_PARSER_CREATE,
        payload: {field, value}
      })
    }
  },

  add (data) {
    return (dispatch)=> {
      dispatch({type: constants.ADD_PARSER_CREATE});

      //initialize requestDispatchType variable for later use
      let requestDispatchType;
      api.add(data)
        .then((response) => {
          //set requestDispatchType variable
          requestDispatchType = response.ok ?
            constants.ADD_PARSER_CREATE_SUCCESS : constants.ADD_PARSER_CREATE_ERROR;
          return response.json()
        })
        .then((json) => {
          dispatch({
            type:    requestDispatchType,
            payload: json
          });

          if (requestDispatchType == constants.ADD_PARSER_CREATE_SUCCESS) {
            dispatch({type: constants.RESET_PARSER_CREATE});
            browserHistory.push('/parsers');
          }
        })
    }
  },

  reset () {
    return (dispatch)=> {
      dispatch({type: constants.RESET_PARSER_CREATE});
    }
  }
};


export default actions
