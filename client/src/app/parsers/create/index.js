import {connect} from 'react-redux';
import view from './view';
import actions from './actions';


let mapStateToProps = (state) => {
  return {
    parser: state.parsers.create
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    update: (field, value)=> {
      dispatch(actions.update(field, value));
    },
    add: (data)=> {
      dispatch(actions.add(data));
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(view)
