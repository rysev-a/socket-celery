export function translateStatus (status) {
  let dictionary = {
    running: 'скариную',
    stopped: 'прервано',
    complete: 'завершено',
    ready: 'настроен',
    error: 'ошибка',
    processing: 'запускаю'
  };

  return dictionary[status];
}
