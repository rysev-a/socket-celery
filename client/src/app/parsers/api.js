import settings from 'settings';


const {API_URL} = settings;
const baseUrl = `${API_URL}/parsers`;

let api = {
  start: (id)=> {
    return fetch(`${baseUrl}/${id}/start`, {
      method: 'post',
      credentials: 'include',
      headers: {'Content-Type': 'application/json'}
    })
  },

  stop: (id)=> {
    return fetch(`${baseUrl}/${id}/stop`, {
      method: 'post',
      credentials: 'include',
      headers: {'Content-Type': 'application/json'}
    })
  },

  list: ()=> {
    return fetch(baseUrl);
  },

  item: (id)=> {
    return fetch(`${baseUrl}/${id}`)
  },

  add: (item) => {
    return fetch(baseUrl, {
      method: 'post',
      body: JSON.stringify(item),
      headers: {'Content-Type': 'application/json'}
    });
  },

  excel: (id) => {
    return fetch(`${baseUrl}/${id}/excel`, {
      method: 'post',
      headers: {'Content-Type': 'application/json'}
    });
  },

  update: (item) => {
    return fetch(`${baseUrl}/${item.id}`, {
      method: 'put',
      body: JSON.stringify(item),
      headers: {'Content-Type': 'application/json'}
    })
  },

  remove: (id)=> {
    return fetch(`${baseUrl}/${id}`, {
      method: 'delete'
    })
  },

  upload: (file)=> {
    let data = new FormData();
    data.append('file', file);

    return fetch('/api/v1/parsers/upload',{
      method: 'post',
      body: data
    });
  }
};

export default api;
