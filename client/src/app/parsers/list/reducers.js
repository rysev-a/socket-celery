import Immutable from 'immutable';
import {combineReducers} from 'redux';
import constants from 'app/constants';

function data (state = [], action) {
  switch (action.type) {
    case constants.FETCH_PARSER_LIST_SUCCESS:
      return action.payload;

    case constants.REMOVE_PARSER_SUCCESS:
      let removedIndex = state.indexOf(state.filter((t)=> t.id == action.payload)[0]);
      return Immutable.fromJS(state).delete(removedIndex).toJS();
    
    case constants.ADD_PARSER_CREATE_SUCCESS:
      return Immutable.fromJS(state).push(action.payload).toJS();

    case constants.UPDATE_PARSER:
      let updateIndex = state.findIndex(x => x.id == action.payload.id);
      let newParse = Object.assign({}, state[updateIndex], action.payload);
      return Immutable.fromJS(state).set(updateIndex, newParse).toJS();

    case constants.UPLOAD_FILE_SUCCESS:
      return [...state, action.payload];

    default:
      return state;
  }
}

export default combineReducers({data})
