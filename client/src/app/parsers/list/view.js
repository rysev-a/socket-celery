import React from 'react';
import {Component} from 'react';
import {Link} from 'react-router';
import {translateStatus} from '../utils';
import Moment from 'moment';


class ParserListView extends Component {
  formatDateTime (dateTime) {
    if (!dateTime) {
      return '-------';
    }
    return Moment(dateTime).format('YY-MM-DD hh:mm');
  }

  render() {
    return <div className="parsers-list">
      <table className="u-full-width parsers-list__table">
        <thead>
          <tr>
            <th>Адрес сайта</th>
            <th>Статус</th>
            <th>Время запуска</th>
            <th>Время завершения</th>
            <th>Запустить</th>
            <th>Остановить</th>
            <th>Удалить</th>
          </tr>
        </thead>
        <tbody>
          {this.showParsers()}
        </tbody>
      </table>
      <div className="parsers-list__buttons">
        <div className="from-excel">Создать парсер из excel файла</div>
        <input type="file"
               onChange={this.upload.bind(this)} />

        <div className="by-form">Создать используя форму</div>
        <Link className="button button-primary"
              to="/parsers/new">Создать</Link>
      </div>        
    </div>;
  }

  upload (e) {
    this.props.upload(e.target.files[0]);
  }

  showParsers () {
    if (this.props.parsers.data.length) {
      return this.props.parsers.data.map(this.parserItem.bind(this));
    } else {
      return <tr><td colSpan='7'>-----</td></tr>
    }
  }

  parserItem (parser) {
    return <tr key={parser.id}>
      <td>
        <Link to={`parsers/${parser.id}`}>{parser.domain}</Link>
      </td>
      <td>{translateStatus(parser.status)}</td>
      <td>{this.formatDateTime(parser.start_datetime)}</td>
      <td>{this.formatDateTime(parser.end_datetime)}</td>
      <td>
        <a className={this.getButtonCss('start', parser)}
           onClick={this.props.start.bind(null, parser.id)}>
             Запустить</a>
      </td>
      <td>
        <a className={this.getButtonCss('stop', parser)}
           onClick={this.props.stop.bind(null, parser.id)}>
             Остановить</a>
      </td>
      <td>
        <a className={this.getButtonCss('remove', parser)}
           onClick={this.props.remove.bind(null, parser.id)}>
             Удалить</a>
      </td>
    </tr>
  }

  getButtonCss (button, parser) {
    let granted = {
      'start': ['complete', 'stopped', 'error', 'ready'],
      'stop': ['running'],
      'remove': ['complete', 'stopped', 'error', 'ready']
    };

    if (granted[button].indexOf(parser.status) != -1) {
      return 'button button-primary active';
    } else {
      return 'button button-primary hidden';
    }
  }

  componentDidMount() {
    if (this.props.parsers.data.length == 0) {
      this.props.fetch();
    }
  }

}

export default ParserListView;
