import constants from 'app/constants';
import api from '../api';

let actions = { 
  fetch: ()=> {
    return (dispatch)=> {
      dispatch({type: constants.FETCH_PARSER_LIST});
      api.list()
        .then((response) => response.json())
        .then((json) => {
          dispatch({
            type: constants.FETCH_PARSER_LIST_SUCCESS,
            payload: json
          });
        })
    }
  },
  
  remove: (id)=> {
    return (dispatch)=> {
      dispatch({type: constants.REMOVE_PARSER});
      api.remove(id)
        .then((response) => {
          if (response.ok) {
            dispatch({
              type: constants.REMOVE_PARSER_SUCCESS,
              payload: id
            });
          }
        })
    }
  },

  start: (id)=> {
    return (dispatch)=> {
      api.start(id)
        .then((response) => {
          if (response.ok) {
            return response.json()            
          }
        })
        .then((json) => {
          dispatch({
            type: constants.UPDATE_PARSER,
            payload: json
          });
        })
    }
  },

  stop: (id)=> {
    return (dispatch)=> {
      api.stop(id)
        .then((response) => {
          if (response.ok) {
            return response.json()            
          }
        })
        .then((json) => {
          dispatch({
            type: constants.UPDATE_PARSER,
            payload: json
          });
        })
    }
  },

  update: (data)=> {
    return ({
      type: constants.UPDATE_PARSER,
      payload: data
    });
  },

  upload: (file)=> {
    return (dispatch) => {
      api.upload(file)
        .then((response) => {
          if (response.ok) {
            return response.json()            
          }
        })
        .then((json) => {
          dispatch({
            type: constants.UPLOAD_FILE_SUCCESS,
            payload: json
          });
        })
    }
  }
}


export default actions
