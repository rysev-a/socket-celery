export default {
  FETCH_PARSER_LIST: 'fetch parser list',
  FETCH_PARSER_LIST_SUCCESS: 'fetch parser list success',
  REMOVE_PARSER: 'remove parser',
  REMOVE_PARSER_SUCCESS: 'remove parser success',
  UPDATE_PARSER: 'update parser',

  UPLOAD_FILE: 'upload file',
  UPLOAD_FILE_SUCCESS: 'upload file success',
  UPLOAD_FILE_ERROR: 'upload file error'
}
