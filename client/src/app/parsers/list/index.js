import {connect} from 'react-redux';
import view from './view';
import actions from './actions';

import socket from 'app/socket';
import store from 'app/store';

socket.on('parsers:update', (parser)=> {
  store.dispatch(actions.update(parser));
});


let mapStateToProps = (state) => {
  return {
    parsers: state.parsers.list
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    fetch: () => dispatch(actions.fetch()),
    remove: (id)=> {
      dispatch(actions.remove(id));
    },
    start: (id)=> {
      dispatch(actions.start(id));
    },
    stop: (id)=> {
      dispatch(actions.stop(id));
    },
    upload: (file)=> {
      dispatch(actions.upload(file));
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(view)
