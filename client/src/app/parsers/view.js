import React from 'react';
import {Component} from 'react';
import {Link} from 'react-router';


class ParserBaseView extends Component {
  render() {
    return <div className="parsers">
      {this.props.children}
    </div>;
  }
}

export default ParserBaseView;
