import React from 'react';
import {Component} from 'react';
import {IndexLink, Link} from 'react-router';


export class AppView extends Component {
  render() {
    let linkProperties = {
      activeClassName: 'active',
      className: 'top-menu__item'
    };

    return <div className="container">
      <nav className="top-menu">
        <IndexLink {...linkProperties} to="/">Главная</IndexLink>
        <Link {...linkProperties} to="/parsers">Парсеры</Link>
        <Link {...linkProperties} to="/distributors">Рассылка</Link>
        <Link {...linkProperties} to="/emails">База адресов</Link>
      </nav>
      {this.props.children}
    </div>;
  }
}

export class StartView extends Component{
  render() {
    return <div>Статистика сканирования</div>;
  }
}
